import React from 'react'
import PortfolioCard from '../PortfolioCard/PortfolioCard'
import './Portfolio.css'

const Portfolio = () => {
  const projects = [
    {
      id: '1',
      title: 'CMS app',
      technologies: 'MERN stack',
      image: ''
    },
    {
      id: '2',
      title: 'Pepe Restaurant',
      technologies: 'React.js, CSS, HTML',
      image: ''
    },
    {
      id: '3',
      title: 'SPA app',
      technologies: 'JavaScript, CSS, HTML',
      image: ''
    },
    {
      id: '4',
      title: 'Landing page',
      technologies: 'Wordpress',
      image: ''
    },
  ]

  return (
    <div id='portfolio' className='app__container-wrapper'>
      <div className="app__container-wrapper_portoflio">
      <PortfolioCard
        title={projects[0].title}
        technologies={projects[0].technologies}
      />
      <PortfolioCard 
        title={projects[1].title}
        technologies={projects[1].technologies}
      />
      <PortfolioCard 
        title={projects[2].title}
        technologies={projects[2].technologies}   
      />
      <PortfolioCard 
        title={projects[3].title}
        technologies={projects[3].technologies}     
      />
      </div>
    </div>
  )
}

export default Portfolio