import React, {useState} from 'react'
import {Link} from 'react-scroll'
import {About, Skills, Portfolio, Contact} from '../index'
import {BsPerson, BsPersonFill, BsPeople, BsPeopleFill, BsBriefcase, BsBriefcaseFill, BsTelephone, BsTelephoneFill, BsArrowUpCircle, BsArrowUpCircleFill} from 'react-icons/bs'

import './Navbar.css'

const Navbar = () => {
  
  return (
    <div className='app__navbar'>
      <ul className='app__navbar-links'>
        
        <Link to="about" spy={true} smooth={true} offset={0} duration={800}>
        <li>
            <BsPerson size={'2em'}/>
        </li>
        </Link>
        
        <Link to="skills" spy={true} smooth={true} offset={0} duration={800}>
          <li>
            <BsPeople size={'2em'}/>
          </li>
        </Link>
        
        <Link to="portfolio" spy={true} smooth={true} offset={0} duration={800}>
          <li>
            <BsBriefcase size={'2em'}/>
          </li>
        </Link>
      
        <Link className='app__navbar-link' to="contact" spy={true} smooth={true} offset={0} duration={800}>
          <li>
            <BsTelephone size={'2em'}/>
          </li>
        </Link>
        
      </ul>
    </div>
  )
}

export default Navbar