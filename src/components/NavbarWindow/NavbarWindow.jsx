import React from 'react'

import './NavbarWindow.css'

const NavbarWindow = () => {
  return (
    <div className='app__navbar-window'>
        <div className='app__navbar-window-active'>
          <h2>window</h2>
        </div>
    </div>
  )
}

export default NavbarWindow