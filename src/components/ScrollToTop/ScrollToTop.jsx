import React, { useEffect, useState } from "react";
import { BsArrowUpCircle, BsArrowUpCircleFill } from 'react-icons/bs'

import './ScrollToTop.css'

const ScrollToTop = () => {
    const [showScrollTopButton, setShowScrollTopButton] = useState(false)

    useEffect(() => {
        window.addEventListener("scroll", () => {
            if(window.scrollY > 900) {
                setShowScrollTopButton(true)
            } else {
                setShowScrollTopButton(false)
            }
        })
    }, [])

    const scrollTop = () => {
        window.scrollTo({
            top: 0,
            behavior: 'smooth'
        });
    };

    return (
        <div className="app__scroll-to-top">
            {showScrollTopButton && 
                <BsArrowUpCircle 
                    size={'2em'} 
                    onClick={scrollTop}
                />
            }
        </div>
    )
}

export default ScrollToTop
