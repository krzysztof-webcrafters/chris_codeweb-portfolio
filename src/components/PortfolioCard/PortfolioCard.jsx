import React from 'react'

import './PortfolioCard.css'


function PortfolioCard (props) {
  return (
    <div className='app__portfolio-card'>
        <img className='app__portfolio-card_image'/>
        <div>
            <p>{props.title}</p>
        </div>
        <div>
            <p>{props.technologies}</p>
        </div>
    </div>
  )
}

export default PortfolioCard