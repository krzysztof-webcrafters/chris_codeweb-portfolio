import NavbarWindow from './NavbarWindow/NavbarWindow'
import Navbar from './Navbar/Navbar'
import ScrollToTop from './ScrollToTop/ScrollToTop'

import Home from './Home/Home'
import About from './About/About'
import Skills from './Skills/Skills'
import Portfolio from './Portfolio/Portfolio'
import Contact from './Contact/Contact'
import Footer from './Footer/Footer'



export {NavbarWindow, Navbar, ScrollToTop, Home, About, Skills, Portfolio, Contact, Footer}