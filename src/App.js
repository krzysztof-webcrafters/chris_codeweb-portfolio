import React from 'react'

import {NavbarWindow, Navbar, ScrollToTop, Home, About, Skills, Portfolio, Contact, Footer} from './components/index'

import './App.css'


const App = () => {

  return (
    <div>
        <Navbar />
        <NavbarWindow />
        <ScrollToTop />
        <Home/>
        <About />
        <Skills />
        <Portfolio />
        <Contact />
        <Footer />
    </div>
  )
}

export default App